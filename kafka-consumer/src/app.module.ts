import { Module } from '@nestjs/common';
import {
  ClientProviderOptions,
  ClientsModule,
  Transport,
} from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import * as dotenv from 'dotenv';

dotenv.config();
const options: ClientProviderOptions = {
  name: 'HERO_SERVICE',
  transport: Transport.KAFKA,
  options: {
    client: {
      clientId: 'kafkaSample',
      brokers: [process.env.BROKER],
    },
  },
};

@Module({
  imports: [ClientsModule.register([options])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
