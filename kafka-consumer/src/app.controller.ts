import { Controller, Get } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern('my-first-topic')
  get(@Payload() message: any) {
    console.log(message.value);
    console.log(message.partition);
    return 'hello world';
  }

  @MessagePattern('failed-topic')
  failedTopic(@Payload() message: any) {
    console.log('failed topic ', message.value);
    return 'failed topic';
  }

  @Get('/hello')
  getHello() {
    this.appService.handledErrors();
    return 'hello';
  }
}
