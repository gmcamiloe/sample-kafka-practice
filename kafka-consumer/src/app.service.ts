import { Inject, Injectable } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';

@Injectable()
export class AppService {
  constructor(@Inject('HERO_SERVICE') private readonly client: ClientKafka) {}

  async onModuleInit() {
    this.client.subscribeToResponseOf('failed-topic');
    await this.client.connect();
  }

  handledErrors(): void {
    this.client.emit('failed-topic', 'hello');
  }
}
