import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';

dotenv.config();

async function bootstrap() {
  const app2 = await NestFactory.create(AppModule);
  const BROKER = process.env.BROKER;
  console.log(BROKER);
  app2.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        brokers: [BROKER],
      },
      consumer: {
        groupId: 'my-kafka-consumer',
        maxBytesPerPartition: 2,
      },
    },
  });


  
  const PORT = Number(process.env.PORT) || 3000;
  await app2.startAllMicroservicesAsync();
  app2.listen(PORT, () => console.log(`initialized ${PORT}`));
}
bootstrap();
