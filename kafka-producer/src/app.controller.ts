import { Controller, Get } from '@nestjs/common';
import { Client, ClientKafka, Transport } from '@nestjs/microservices';
import { AppService } from './app.service';
import * as dotenv from 'dotenv';

dotenv.config()

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Client({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: 'kafkaSample',
        brokers: [process.env.BROKER],
      },
      consumer: {
        groupId: 'my-kafka-consumer',
      },
    },
  })
  client: ClientKafka;

  async onModuleInit() {
    // Need to subscribe to topic
    // so that we can get the response from kafka microservice
    this.client.subscribeToResponseOf('my-first-topic');
    await this.client.connect();
  }

  @Get()
  async getHello() {
    this.client.emit('my-first-topic', {
      user: 'camilo',
    }); // args - topic, message

    return 'xd';
  }
}
