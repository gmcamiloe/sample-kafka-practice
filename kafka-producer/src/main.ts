import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';

dotenv.config();

async function bootstrap() {
  console.log(process.env.BROKER);
  const app = await NestFactory.create(AppModule);
  const PORT = Number(process.env.PORT) || 3000;
  await app.listen(PORT, () => console.log(`the servier is listening ${PORT}`));
}
bootstrap();
